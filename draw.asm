global draw

section .data
	heap db "                            Heap",10
	heaplen equ $ - heap

	stack db "                           Stack",10
	stacklen equ $ - stack

	error db "Error!", 10		;error message
	errorlen equ $ - error      ;error length

	space_c db ' '				;space message

	nl db 10					;new line
	nllen equ $ - nl 			;new line length

	charac db 35                ;character #

	reset db 27,"[0;1m",0       ;rreset color code
	resetlen equ $ - reset 		;reset color code length

	red db 27,"[31;1m",0        ;red color code
	redlen equ $ - red 			;red color code length

	green db 27,"[32;1m",0 		;green color code
	greenlen equ $ - green   	;green color code length

	cyan db 27,"[34;1m",0 		;cyan color code
	cyanlen equ $ - cyan   	    ;cyan color code length

section .text
draw:	
	mov r8, rdx					;heap or stack
	mov r9, 0					;var for row spacing
	mov r10, rdi 				;r10 -> map address
	mov r11, rsi				;r11 -> size

	cmp r8, 1
	je Loop

	call printNl				;calls for spacing
	call printNl
	call printNl
	call printNl
	call printNl
	call setCyan
	call printHeap

Loop:
	cmp r9, 60
	je new_line

	cmp byte [r10], 32
	je space

	cmp byte [r10], 35
	je tag

	cmp byte [r10], 43
	je plus

	cmp byte [r10], 45
	je minus

	cmp byte [r10], 0
	je last_line

	cmp r11, 0
	je last_line

;-----------------color and write characters-----------------------
tag:
	call setReset
	call printChar
	jmp next

plus:
	cmp r8, 1
	je plus_stack

	call setCyan
	call printChar
	jmp next

plus_stack:
	call setGreen
	call printChar
	jmp next

minus:
	call setRed
	call printChar
	jmp next

space:
	call printSpace
	jmp next

;--------------------------inc and decr---------------------------
next:
	inc r10
	dec r11
	inc r9
	cmp r11, 0
	jne Loop

last_line:
	call printNl
	cmp r8, 1
	je mstack
	ret

mstack:
	call setGreen
	call printStack
	ret

;--------------------------new_line-----------------------------	
new_line:
	mov r9, 0
	call printNl
	jmp Loop

;------------------------functions-----------------------------

setRed:
	mov rax, 1
	mov rdi, 1
	mov rsi, red
	mov rdx, redlen
	syscall
	ret

setGreen:
	mov rax, 1
	mov rdi, 1
	mov rsi, green
	mov rdx, greenlen
	syscall
	ret

setCyan:
	mov rax, 1
	mov rdi, 1
	mov rsi, cyan
	mov rdx, cyanlen
	syscall
	ret

setReset:
	mov rax, 1
	mov rdi, 1
	mov rsi, reset
	mov rdx, resetlen
	syscall
	ret

printChar:
	mov rax, 1
	mov rdi, 1
	mov rsi, charac
	mov rdx, 1
	syscall
	ret

printNl:
	mov rax, 1
	mov rdi, 1
	mov rsi, nl
	mov rdx, nllen
	syscall
	ret

printErr:
	mov rax, 1
	mov rdi, 1
	mov rsi, error
	mov rdx, errorlen
	syscall
	ret

printHeap:
	mov rax, 1
	mov rdi, 1
	mov rsi, heap
	mov rdx, heaplen
	syscall
	ret

printSpace:
	mov rax, 1
	mov rdi, 1
	mov rsi, space_c
	mov rdx, 1
	syscall
	ret

printStack:
	mov rax, 1
	mov rdi, 1
	mov rsi, stack
	mov rdx, stacklen
	syscall
	ret
