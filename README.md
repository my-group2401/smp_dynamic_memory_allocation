#                      Alocarea Memorier Dinamice

    In acest proiect am incercat sa urmaresc folosirea sistemelor de alocare dinamica(malloc() si free()) si de a le reprezenta in mod grafic.Astfel am 3 fisiere, unul c principal si alte 3 scrise in assembly.


## dynamic_memory.c

    In fiserul principal am setat 2 limite la marimea stackului si a heapului ale fisierului respectiv cu ajutorul functiei setrlimit() din biblioteca<sys/resources.h>.
    Dupa aceea am citit din fisierul "/proc/self/maps" ce contine infoormatii despre adresele de memorie alocate de acel fisier in sine.
    In datele citite, am cautat adresele de inceput si de sfarsit ale stackului si ale heapului.

    Dupa ce au fost gasite am inceput alocarea de memorie atat dinamica cat si  statica. Am alocat cativa vectori de tip char pentru a avea o stocare byte cu byte. 
    Toti vectorii au fost initializati cu o anumita valoare proprie. De exemplu vec1 a fost umplut cu valoarea 11, vec2 cu valoarea 155 s.a.m.d.
	

## verify.asm

    In acest fisier assembly am luat fiecare vector dinamic si l.am verificat  daca a fost alocat asa cum ar trebui sa fie alocat de catre malloc().
    Astfel am luat adresa de inceput a vectorului, si dimensiunea de memorie ce a fost alocata.
    Am parcurs de la inceputul acestuia pana lasfarsitul lui adresele de memorie, si amm verificat daca contin valoarea specifica vectorului. Daca da functia returna true daca nu, false.

## mapping.asm

    Pentru a demonstra grafic modul de alocare dinamica am folosit doua "matrice". Ambele au reprezentara memoriei de 1 la 100. Deci 1 element din matrice valoreaza de fapt 100 de adrese de memorie.
    Prima matrice este heapul iar cea dea doua este stackul.
    Astfel pentru fiecare vector am mapat in cele 2 matrice locurile de memorie arpoximative.
    Daca vectorul era alocat dinamic in matrice adaugam un '+' iar daca era eliberat adaugam un '-'.

## draw.asm

    Tot in assembly am implementat si afisarea grafica in trminal.
    Primeam ca si atribute adresa matricii si o parcurgeam astfel unde dadeam de un bloc de memorie nealocat afisam tot un bloc de memorie nealocat, unde  era memorie alcata blocul devenea in heap albastru iar in stack verde, iar unde memoria era eliberata blocurile de memorie se transformau in rosu.
    Astfel avem legenda:
* -# pentru blocurile initiale de la pornirea prgramului
* -#(verde) pentru blocurile de memorie alocate in stack
* -#(albastru) pentru blocurile de memorie alocate dinamic
* -#(rosu) pentru blocurile de memorie eliberate


    Ca si rulare, am afisat harta efectiva a stack-ului si heap-ului, care se updateaza prin citirea unei variabile aleatoare.

![alt text](./Screenshot_from_2021-06-04_10-12-48.png)

## Bibliografie:
	-<https://en.wikipedia.org/wiki/Netwide_Assembler>
	-<https://stackoverflow.com/>
	-<https://linux.die.net/man/>
	-<https://stackoverflow.com/questions/3501338/c-read-file-line-by-line>
	-<https://www.log2base2.com/C/pointer/malloc-in-c.html>
	-<https://core.ac.uk/download/pdf/189667001.pdf>
	-<https://nikosmouzakitis.medium.com/c-code-arm-assembly-emulating-execution-using-qemu-f2d63a3f40ca>
	-<https://stackoverflow.com/questions/15802748/arm-assembly-using-qemu>
	-<https://azeria-labs.com/arm-on-x86-qemu-user/>
