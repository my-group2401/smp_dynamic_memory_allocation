global verify

section .data

section .text

verify:
	cmp byte [rdi], dl
	jne err

	inc rdi
	dec rsi

	cmp rsi, 1
	jne verify

	cmp rsi, 1
	je ok

err:
	mov rax, 0
	ret

ok:
	mov rax, 1
	ret