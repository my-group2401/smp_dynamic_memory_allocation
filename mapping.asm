global mapping

section .data

section .text

mapping:
	mov r10, rdi
	add rdi, rsi

	cmp rcx, 0
	je free_mapping

alloc_mapping:	
	mov byte [rdi], '+'
	inc rdi
	dec rdx
	cmp rdx, 0
	jne alloc_mapping

	mov rax, r10
	ret

free_mapping:
	mov byte [rdi], '-'
	inc rdi
	dec rdx
	cmp rdx, 0
	jne free_mapping

	mov rax, r10
	ret