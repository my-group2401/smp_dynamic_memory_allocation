dynamic: dynamic_memory.c mapping.asm verify.asm draw.asm
	gcc -c -m64 -O2 -ggdb dynamic_memory.c	
	nasm -f elf64 -g -F dwarf mapping.asm
	nasm -f elf64 -g -F dwarf draw.asm
	nasm -f elf64 -g -F dwarf verify.asm
	gcc -m64 dynamic_memory.o mapping.o draw.o verify.o -o res

run:
	./res

clean:
	rm -f mapping *.o *.s
	rm -f verify *.o *.s
	rm -f draw *.o *.s
	rm -f res
	rm -f dynamic_memory *.o *.s
