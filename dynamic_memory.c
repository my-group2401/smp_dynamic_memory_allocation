#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>

extern char* mapping(char*, int, int, int);
extern void draw(char*, int, int);
extern int verify(char*, int, int);

char* test(char * X, int size, char* map, char* map_stack, char* heap_beg, int what){

	int tst;
	if(scanf("%d",&tst))

	map = mapping(map, ((unsigned int)(X - heap_beg))/100, size/100,what);
	draw(map, 1352, 0);
	draw(map_stack, 1386, 1);
	
	return map;
}

int main(){
	
	//-----------------------------------Setting Limits----------------------------------//

	struct rlimit r_lim;
	getrlimit(RLIMIT_DATA, &r_lim);
	r_lim.rlim_cur = 1024*176;
	r_lim.rlim_max = 1024*176;
	setrlimit(RLIMIT_DATA, &r_lim);

	getrlimit(RLIMIT_STACK, &r_lim);
	r_lim.rlim_cur = 1024*176;
	r_lim.rlim_max = 1024*176;
	setrlimit(RLIMIT_STACK, &r_lim);

	//------------------------------------Memory Segments---------------------------------//

	char buffer[1024], aux[13];
	char * is_mem,* heap_beg,* heap_end,* stack_beg,* stack_end; 
	unsigned long long ul;
	int pos, c, tst;

  	FILE *f = fopen("/proc/self/maps", "r");
  	if(f){
    	do{
    		//init
      		pos = 0;
      		aux[13] = '\0';

      		//read line
      		do{
      			c = fgetc(f);
      			if(c != EOF) 
      				buffer[pos++] = (char)c;
      		}while(c != EOF && c != '\n');
      		buffer[pos] = 0;

      		//search for heap
  	  		is_mem = strstr(buffer, "heap");
  	  		if (is_mem != NULL){
  		  		//print heap
  				printf("%s",buffer);

  				//copy heap_begin
    			strncpy(aux,buffer,12);
    			sscanf(aux, "%llx", &ul);
  				heap_beg = (char*)(uintptr_t) ul;

    			//copy heap_end
    			strncpy(aux, buffer + 13,12);
    			sscanf(aux, "%llx", &ul);
  				heap_end = (char*)(uintptr_t) ul;
  	  		}

  			//search for stack
  			is_mem = strstr(buffer, "stack");
  			if (is_mem != NULL){
  				//print stack
  				printf("%s",buffer);

  				//copy stack_begin
    			strncpy(aux, buffer,12);
    			sscanf(aux, "%llx", &ul);
  				stack_beg = (char*)(uintptr_t) ul;

    			//copy stack_end
    			strncpy(aux, buffer + 13,12);
    			sscanf(aux, "%llx", &ul);
  				stack_end = (char*)(uintptr_t) ul;
  			}
    	}while(c != EOF); 
    	fclose(f);           
  	}

  	//----------------------------------------Allocatig Memory-------------------------------//

  	char * A = (char*)malloc(10000*sizeof(char));
	for(int i = 0; i< 10000; i++){
		A[i] = 111;
	}
	if(!verify(A, 10000, A[0]))
		printf("Bad Allocation!\n");

	char * B = (char*)malloc(20000*sizeof(char));
	for(int i = 0; i< 20000; i++){
		B[i] = 122;
	}
	if(!verify(B, 20000, B[0]))
		printf("Bad Allocation!\n");

	char * C = (char*)malloc(20000*sizeof(char));
	for(int i = 0; i< 20000; i++){
		C[i] = 123;
	}
	if(!verify(C, 20000, C[0]))
		printf("Bad Allocation!\n");

	char * D = (char*)malloc(20000*sizeof(char));
	for(int i = 0; i< 20000; i++){
		D[i] = 22;
	}
	if(!verify(D, 20000, D[0]))
		printf("Bad Allocation!\n");

	char * E = (char*)malloc(20000*sizeof(char));
	for(int i = 0; i< 20000; i++){
		E[i] = 33;
	}
	if(!verify(E, 20000, E[0]))
		printf("Bad Allocation!\n");

	free(C);
	free(D);

	char * F = (char*)malloc(38000*sizeof(char));
	for(int i = 0; i< 38000; i++){
		F[i] = 111;
	}
	if(!verify(F, 38000, F[0]))
		printf("Bad Allocation!\n");

	//-------------------------------Maps--------------------------------

	char *map = (char*)malloc(1352*sizeof(char));
	for(int i = 0; i < 1352 ; i++){
		map[i] = '#';
	}
	
	char *map_stack = (char*)malloc(1386*sizeof(char));
	for(int i = 0; i < 34 ; i++){
		map_stack[i] = ' ';
	}
	for(int i = 34; i < 1386 ; i++){
		map_stack[i] = '#';
	}

	//-----------------------------Allocating more Memoy--------------------------------

	char * G = (char*)malloc(5000*sizeof(char));
	for(int i = 0; i< 5000; i++){
		G[i] = 11;
	}
	if(!verify(G, 5000, G[0]))
		printf("Bad Allocation!\n");

	char ff[40000];

	//--------------------------Testing-----------------------------

	

	map = test(A,10000,map,map_stack,heap_beg,1);
	map = test(B,20000,map,map_stack,heap_beg,1);
	map = test(C,20000,map,map_stack,heap_beg,1);
	map = test(D,20000,map,map_stack,heap_beg,1);
	map = test(E,20000,map,map_stack,heap_beg,1);
	map = test(C,20000,map,map_stack,heap_beg,0);
	map = test(D,20000,map,map_stack,heap_beg,0);
	map = test(F,38000,map,map_stack,heap_beg,1);
	map = test(G,5000,map,map_stack,heap_beg,1);

	if(scanf("%d",&tst))
		;
	map_stack = mapping(map_stack, ((unsigned int)(buffer - stack_beg))/100, 2,1);
	draw(map, 1352, 0);
	draw(map_stack, 1386, 1);
	if(scanf("%d",&tst))
	map_stack = mapping(map_stack, ((unsigned int)(ff - stack_beg))/100, 400,1);
	draw(map, 1352, 0);
	draw(map_stack, 1386, 1);
	if(scanf("%d",&tst))
		;

	//-----------------------------------Allocating Over the Limit-------------------------

	char* H = (char*)malloc(40000*sizeof(char));
	if(H == NULL)
		printf("OVERFLOW\n");


	return 0;
}